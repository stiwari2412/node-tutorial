//   ------first program-------   
function sayHello(name)
{
  console.log('Hello '+name);
}
sayHello("Sunny");
console.log(window);

//-------exporting a module-------
const logger=require('./logger');
logger.log('message');

//------exporting a single function-------
const logger=require('./logger');
logger('message');

//--------path module
const path=require('path');
var pathObject=path.parse(__filename);
console.log(pathObject);
// --------os module
const os=require('os');
var totalMemory=os.totalmem();
var freeMemory=os.freemem();
console.log('Total memory:'+totalMemory);
console.log('free memory:'+freeMemory);
// ---------filesystem module
const fs=require('fs');
const files=fs.readdirSync('./');
console.log(files); 
fs.readdir('./',function(err,files){
  if(err) console.log('Error',err);
  else console.log('Result',files);
});
// --------events module
const EventEmitter=require('events')
const emitter=new EventEmitter();
//Register event
emitter.on('messageLogged',function(arg){
  console.log('event called',arg);
})
emitter.emit('messageLogged',{id:1,url:'https://'});
const EventEmitter=require('events')
const Logger=require('./logger');
const logger=new Logger();
//const url='http://www.google.com';
logger.on('messageLogged',function(arg){
  console.log('event called',arg);
});


logger.log('message');

//------------http module
const http=require('http');
const server=http.createServer((req,res)=>{
  if(req.url==='/'){
    res.write('hello ')
    res.end();
  }
  if(req.url==='/api')
  {
    res.write(JSON.stringify([1,2,3]));
    res.end();
  }
});
//server.on('connection',(socket)=>{console.log("hello");})
server.listen(3000);
console.log('listening on 3000'); 